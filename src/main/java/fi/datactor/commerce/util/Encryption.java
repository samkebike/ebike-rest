package fi.datactor.commerce.util;

import fi.datactor.commerce.api.exceptions.AbstractControllerException;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by rikuusi on 27.12.2016.
 */
public class Encryption {

    private final Logger log = LoggerFactory.getLogger(Encryption.class);
    private String initVector;
    private String key;

    /**
     * Sets Encryption Vector
     * @param initVector
     */
    public void setInitVector(String initVector) {
        this.initVector = initVector;
    }

    /**
     * Sets Encryption Key
     * @param key
     */
    public void setKey(String key) {
        this.key = key;
    }



    /**
     * Encrypts String value
     * @param value
     * @return
     * @throws AbstractControllerException
     */
    public String encrypt(String value){
        try {

            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));



            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());


            return Base64.encodeBase64URLSafeString(encrypted);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException ex) {
            log.error("Stack {}",ex);
        }
        return null;
    }

    /**
     * Decrypts
     * @param encrypted
     * @return
     * @throws AbstractControllerException
     */
    public String decrypt(String encrypted){
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));

            return new String(original);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException ex) {
            log.error("Stack {}",ex);
        }

        return null;
    }
}
