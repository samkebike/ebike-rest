package fi.datactor.commerce.api.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import fi.datactor.commerce.util.Blob;

import java.util.UUID;

/**
 * Created by rikuusi on 15.2.2017.
 */
public class AbstractEntity {

    public AbstractEntity(){
        this.setSId(UUID.randomUUID().toString());
    }

    public AbstractEntity(String sid){
        this.setSId(sid);
    }

    protected byte[] id;

    public byte[] getId() {
        return id;
    }

    public void setId(byte[] id) {
        this.id = id;
    }

    @JsonProperty("id")
    public String getSId() {
        return Blob.bytesToHex(id);
    }

    @JsonProperty("id")
    public void setSId(String id) {
        if(id == null){
            setSId(UUID.randomUUID().toString());
        } else {
            this.id = Blob.hexToBytes(id.replace("-", ""));
        }

    }
}
