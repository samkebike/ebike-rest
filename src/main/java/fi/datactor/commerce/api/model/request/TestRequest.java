package fi.datactor.commerce.api.model.request;

/**
 * Created by rikuusi on 6.2.2017.
 */
public class TestRequest {

    private String id;
    private String client;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }
}
