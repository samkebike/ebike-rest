/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datactor.commerce.api.model.error;

import com.fasterxml.jackson.annotation.*;
import java.util.*;

/**
 * List of underlying details
 * @author Riku Kuusisto
 */

@JsonInclude(JsonInclude.Include.ALWAYS)
public class UnderlyingDetails {
    @JsonProperty("causes")
    private List<String> causes = new ArrayList<String>();

    @JsonProperty("causes")
    public void setCauses(List<String> causes) {
        this.causes = causes;
    }

    @JsonProperty("causes")
    public List<String> getCauses() {
        return causes;
    }
}
