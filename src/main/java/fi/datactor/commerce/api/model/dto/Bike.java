package fi.datactor.commerce.api.model.dto;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Iterator;
import java.util.Set;

@Entity
@Table(name = "Bike", schema = "eBike")
public class Bike extends AbstractEntity {

    private Station station;
    private Long nr;
    private byte locked;
    private Set<Log> logs;
    private Set<Reservation> reservations;

    public Bike(){
        super();
    }

    public Bike(String sid){
        super(sid);
    }

    @Id
    public byte[] getId() {
        return id;
    }

    public void setId(byte[] id) {
        this.id = id;
    }

    @JoinColumn(name = "station")
    @OneToOne(fetch = FetchType.EAGER)
    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }


    @GeneratedValue(strategy = GenerationType.AUTO)
    @GenericGenerator(name = "trigger", strategy = "org.hibernate.id.SelectGenerator")
    public Long getNr() {
        return nr;
    }

    public void setNr(Long nr) {
        this.nr = nr;
    }

    public byte getLocked() {
        return locked;
    }

    public void setLocked(byte locked) {
        this.locked = locked;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "client")
    @JsonManagedReference(value = "bikelog")
    public Set<Log> getLogs() {
        return this.logs;
    }

    public void setLogs(Set<Log> logs) {
        this.logs = logs;
    }

    @JsonManagedReference(value = "bikeres")
    @OneToMany(targetEntity = Reservation.class, fetch = FetchType.EAGER, mappedBy = "bike")
    public Set<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(Set<Reservation> reservations) {
        this.reservations = reservations;
    }


    @Transient
    @JsonManagedReference("activeReservation")
    public Reservation getActiveReservation() {
        if (reservations != null)
            for (Iterator<Reservation> it = getReservations().iterator(); it.hasNext(); ) {
                Reservation r = it.next();
                if (r.isActive()) {
                    return r;
                }
            }
        return null;
    }

}
