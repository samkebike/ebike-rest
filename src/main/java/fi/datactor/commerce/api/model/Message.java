/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datactor.commerce.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

/**
 *
 * @author Riku Kuusisto
 */
public class Message<E> {
   @JsonProperty("identification")
    private UUID identification;

    
    @JsonProperty("status")
    private String status;
    @JsonProperty("code")
    private String code;
    @JsonProperty("error")
    private String error;
    @JsonProperty("payload")
    private E payload;

    public String getCode() {
        return code;
    }

    public String getError() {
        return error;
    }

    public E getPayload() {
        return payload;
    }

    public String getStatus() {
        return status;
    }
    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }
    @JsonProperty("error")
    public void setError(String error) {
        this.error = error;
    }
    @JsonProperty("payload")
    public void setPayload(E payload) {
        this.payload = payload;
    }
    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("identification")
    public void setIdentification(UUID identification) {
        this.identification = identification;
    }

    @JsonProperty("identification")
    public UUID getIdentification() {
        return identification;
    }

            
}
