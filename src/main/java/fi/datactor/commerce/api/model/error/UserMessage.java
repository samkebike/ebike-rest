/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datactor.commerce.api.model.error;

import com.fasterxml.jackson.annotation.*;

/**
 * Displayed message
 * @author Riku Kuusisto
 */
public class UserMessage {

    @JsonProperty("text")
    private String text;


    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

}
