package fi.datactor.commerce.api.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "Station", schema = "eBike")
public class Station extends AbstractEntity{

    private String name;
    private Point location;
    private Set<Bike> bikes;
    private String imageUrl;
    private Set<Log> log;

    public Station() {
        super();
    }

    public Station(String sid){
        super(sid);
    }

    @Id
    public byte[] getId() {
        return id;
    }

    public void setId(byte[] id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "location")
    @Type(type="org.hibernate.spatial.GeometryType")
    public Point getLocation() {
        return location;
    }

    @Column(name = "location")
    public void setLocation(Point location) {
        this.location = location;
    }

    @Transient
    @JsonProperty("location")
    public List<Double> getLocationArr(){
        List<Double> out = new ArrayList<>();
        out.add(location.getCoordinate().x);
        out.add(location.getCoordinate().y);
        return out;
    }

    private void setLocation(String location) throws ParseException {
        Geometry g;
        WKTReader reader = new WKTReader();
        g = reader.read(location);
        this.setLocation((Point)g);
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @OneToMany(mappedBy = "station", fetch = FetchType.EAGER)
    @JsonManagedReference("stationlog")
    public Set<Log> getLog() {
        return log;
    }

    public void setLog(Set<Log> log) {
        this.log = log;
    }

    @OneToMany(mappedBy = "station", fetch = FetchType.LAZY)
    //@JsonManagedReference(value = "bikes")
    @JsonIgnore
    public Set<Bike> getBikes(){
        return bikes;
    }
    public void setBikes(Set<Bike> bikes){
        this.bikes =  bikes;
    }
}
