/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datactor.commerce.api.model.error;

import com.fasterxml.jackson.annotation.*;


/**
 * EDS Error message
 * @author Riku Kuusisto
 */
@JsonInclude(JsonInclude.Include.ALWAYS)
public class ErrorMessage{

    @JsonProperty("userMessage")
    private UserMessage userMessage;
    @JsonProperty("underlyingDetails")
    private UnderlyingDetails underlyingDetails;

    @JsonProperty("userMessage")
    public UserMessage getUserMessage() {
        return userMessage;
    }

    @JsonProperty("userMessage")
    public void setUserMessage(UserMessage userMessage) {
        this.userMessage = userMessage;
    }

    @JsonProperty("underlyingDetails")
    public UnderlyingDetails getUnderlyingDetails() {
        return underlyingDetails;
    }

    @JsonProperty("underlyingDetails")
    public void setUnderlyingDetails(UnderlyingDetails underlyingDetails) {
        this.underlyingDetails = underlyingDetails;
    }

}
