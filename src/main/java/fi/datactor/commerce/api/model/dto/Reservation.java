package fi.datactor.commerce.api.model.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;

@Entity
@Table(name = "Reservation", schema = "eBike")
public class Reservation extends AbstractEntity {
  private Customer customer;
  private Bike bike;
  private java.sql.Timestamp validUntil;
  private java.sql.Timestamp reservedUntil;
  private byte active;

  private final Logger log = LoggerFactory.getLogger(Reservation.class);

  public Reservation() {
    super();
  }

  public Reservation(String sid){
    super(sid);
  }

  @Id
  public byte[] getId() {
    return id;
  }

  public void setId(byte[] id) {
    this.id = id;
  }

  @ManyToOne(targetEntity = Customer.class, fetch = FetchType.EAGER)
  @JoinColumn(name = "customer")
  @JsonBackReference("customerres")
  @JsonProperty("customer")
  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  @JoinColumn(name = "bike")
  @ManyToOne(targetEntity = Bike.class, fetch = FetchType.EAGER)
  @JsonBackReference("bikeres")
  @JsonProperty("bike")
  public Bike getBike() {
    return bike;
  }

  public void setBike(Bike bike) {
    this.bike = bike;
  }

  public java.sql.Timestamp getValidUntil() {
    return validUntil;
  }

  public void setValidUntil(java.sql.Timestamp validUntil) {
    this.validUntil = validUntil;
  }

  public java.sql.Timestamp getReservedUntil() {
    return reservedUntil;
  }

  public void setReservedUntil(java.sql.Timestamp reservedUntil) {
    this.reservedUntil = reservedUntil;
  }

  public boolean isActive() {
    return active == (byte)1;
  }

  public void setActive(boolean val) {
    this.active = val ? (byte)1 : (byte)0;
  }
}
