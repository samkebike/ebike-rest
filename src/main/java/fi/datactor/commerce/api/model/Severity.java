package fi.datactor.commerce.api.model;

/**
 * Created by rikuusi on 13.2.2017.
 */

public enum Severity {
    ERROR("ERROR"),
    WARNING("WARNING"),
    INFO("INFO"),
    DEBUG("DEBUG");

    private final String text;

    Severity(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
