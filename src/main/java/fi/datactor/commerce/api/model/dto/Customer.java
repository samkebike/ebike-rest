package fi.datactor.commerce.api.model.dto;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Iterator;
import java.util.Set;

@Entity
@Table(name = "Customer", schema = "eBike")
public class Customer extends AbstractEntity {
    private String firstName;
    private String lastName;
    private String socialId;
    private String cardId;
    private Set<Log> log;

    private Set<Reservation> reservations;

    public Customer() {
        super();
    }

    public Customer(String sid) {
        super(sid);
    }

    @Id
    public byte[] getId() {
        return id;
    }

    public void setId(byte[] id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    @OneToMany(mappedBy = "customer", fetch = FetchType.EAGER)
    @JsonManagedReference("customerres")
    public Set<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(Set<Reservation> reservations) {
        this.reservations = reservations;
    }

    @OneToMany(mappedBy = "customer", fetch = FetchType.EAGER)
    @JsonManagedReference("customerlog")
    public Set<Log> getLog() {
        return log;
    }

    public void setLog(Set<Log> log) {
        this.log = log;
    }

    @Transient
    @JsonManagedReference("activeReservation")
    public Reservation getActiveReservation() {
        if (reservations != null)
            for (Iterator<Reservation> it = getReservations().iterator(); it.hasNext(); ) {
                Reservation r = it.next();
                if (r.isActive()) {
                    return r;
                }
            }
        return null;
    }
}
