package fi.datactor.commerce.api.model.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import fi.datactor.commerce.api.model.Severity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "Log", schema = "eBike")
public class Log extends AbstractEntity {
    private java.sql.Timestamp date;
    private Severity severity;
    private Bike client;
    private Customer customer;
    private Station station;
    private String message;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public Log() {
        super();
    }

    public Log(String sid){
        super(sid);
    }

    @Id
    public byte[] getId() {
        return id;
    }

    public void setId(byte[] id) {
        this.id = id;
    }

    public java.sql.Timestamp getDate() {
        return date;
    }

    public void setDate(java.sql.Timestamp date) {
        this.date = date;
    }

    @Transient
    @JsonProperty("date")
    public String getDateJSON(){
        if(date != null){
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            return sdf.format(new Date(date.getTime()));
        } else{
            return "";
        }

    }

    @Enumerated(EnumType.STRING)
    public Severity getSeverity() {
        return severity;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    @JoinColumn(name = "client", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("bikelog")
    public Bike getClient() {
        return client;
    }

    public void setClient(Bike client) {
        this.client = client;
    }

    @JoinColumn(name = "customer", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("customerlog")
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @JoinColumn(name = "station", insertable = false, nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("stationlog")
    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
