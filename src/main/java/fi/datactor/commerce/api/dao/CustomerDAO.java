package fi.datactor.commerce.api.dao;

import fi.datactor.commerce.api.model.dto.Customer;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by rikuusi on 14.2.2017.
 */
public class CustomerDAO extends AbstractDAO {
    public List<Customer> retrieveAll() {
        Session s = getSession();
        List<Customer> q = s.createQuery("FROM " + Customer.class.getCanonicalName() + " c ORDER BY c.firstName, c.lastName").list();
        s.close();
        return q;
    }

    public Customer fetchInstanceByCardId(String id){
        Session s = getSession();
        return (Customer) getFirst(s.createQuery("FROM " +  Customer.class.getCanonicalName() + " c WHERE c.cardId = :cardId").setString("cardId", id).list());
    }
}
