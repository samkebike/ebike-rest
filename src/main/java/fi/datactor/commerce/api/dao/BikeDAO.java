package fi.datactor.commerce.api.dao;

import fi.datactor.commerce.api.model.dto.Bike;
import fi.datactor.commerce.api.model.dto.Station;
import fi.datactor.commerce.util.Blob;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by rikuusi on 14.2.2017.
 */
public class BikeDAO extends AbstractDAO {

    Logger log = LoggerFactory.getLogger(this.getClass());

    public List<Bike> retrieveAll() {
        Session s = getSession();
        List<Bike> q = s.createQuery("FROM " + Bike.class.getCanonicalName() + " b ORDER BY b.station.name, b.nr").list();
        s.close();
        return q;
    }

    public List<Bike> retrieveAvailable() {
        Session s = getSession();
        List<Bike> q = s.createQuery("FROM " + Bike.class.getCanonicalName() + " b WHERE b.locked = b'0' ORDER BY b.station.name, b.nr").list();
        s.close();
        return q;
    }

    public List<Bike> retrieveFrom(String stationId) {
        Session s = getSession();
        List<Bike> q = s.createQuery("FROM " + Bike.class.getCanonicalName() + " b WHERE b.station.id = uuidtobin(:stationId) ORDER BY b.station.name, b.nr").setString("stationId", stationId).list();
        s.close();
        return q;
    }

    public Bike fetchInstance(String id) {
        /*Session s = getSession();
        List<Bike> q = s.createQuery("FROM " + Bike.class.getCanonicalName() + " b WHERE b.id = uuidtobin(:id)").setString("id", id).list();
        s.close();
        return getFirst(q);*/
        return fetchInstance(Blob.hexToBytes(id));
    }

    public Bike fetchInstance(byte[] id) {
        Session s = getSession();
        //List<Bike> q = s.createQuery("FROM " + Bike.class.getCanonicalName() + " b WHERE b.id = :id").setBinary("id", id).list();
        Bike b = (Bike) s.get(Bike.class, id);
        s.close();
        //return getFirst(q);
        return b;
    }

    public Bike insert(Bike o) {
        Session s = getSession();
        Station station = (Station)s.get(Station.class, o.getStation().getId());
        if(station == null){
            o.setStation(this.insert(o.getStation()));
        } else {
            o.setStation(station);
        }
        o = super.insert(o);
        o = (Bike)s.get(Bike.class, o.getId());
        return o;
    }
}
