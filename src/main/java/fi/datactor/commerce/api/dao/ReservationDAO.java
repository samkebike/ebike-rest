package fi.datactor.commerce.api.dao;

import fi.datactor.commerce.api.model.dto.Reservation;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by rikuusi on 14.2.2017.
 */

public class ReservationDAO extends AbstractDAO {
    public List<Reservation> retrieveAll() {
        Session s = getSession();
        List<Reservation> q = s.createQuery("FROM " + Reservation.class.getCanonicalName() + " r ORDER BY r.reservedUntil DESC").list();
        s.close();
        return q;
    }

    public List<Reservation> retrieveActive() {
        Session s = getSession();
        List<Reservation> q = s.createQuery("FROM " + Reservation.class.getCanonicalName() + " r WHERE r.active = 1 ORDER BY r.reservedUntil").list();
        s.close();
        return q;
    }

}
