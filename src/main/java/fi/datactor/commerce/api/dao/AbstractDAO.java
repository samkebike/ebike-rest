package fi.datactor.commerce.api.dao;

import fi.datactor.commerce.api.model.dto.AbstractEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

import java.util.List;
import java.util.UUID;

/**
 * Created by kuusri on 3/20/2016.
 */
public abstract class AbstractDAO implements ApplicationContextAware {
    private Logger log = LoggerFactory.getLogger(AbstractDAO.class);
    private ApplicationContext ctx;


    public <T> T getFirst(List<T> l) {
        if (l.size() > 0) {
            return l.get(0);
        } else {
            return null;
        }
    }

    public <T> List<T> retrieveAll(T a) {
        Session s = getSession();
        List<T> q = s.createQuery("FROM " + a.getClass().getCanonicalName()).list();
        s.close();
        return q;
    }


    public <T extends AbstractEntity> T edit(T o) {
        Session s = getSession();
        Transaction tx = s.beginTransaction();
        try {
            s.update(o);
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            throw ex;
        }
        return o;
    }

    public <T extends AbstractEntity> T edit(T o, Session s) {
        s.update(o);
        return o;
    }

    public <T extends AbstractEntity> T insert(T o) {
        Session s = getSession();
        Transaction tx = s.beginTransaction();
        try {
            s.persist(o);
            tx.commit();
            s.close();
        } catch (Exception ex) {
            tx.rollback();
            s.close();
            throw ex;
        }
        return o;
    }

    public <T extends AbstractEntity> T insert(T o, Session s) {
        if (o.getId() == null) {
            o.setSId(UUID.randomUUID().toString());
        }
        s.persist(o);
        return o;
    }

    public <T extends AbstractEntity> T delete(T o) {
        Session s = getSession();
        Transaction tx = s.beginTransaction();
        try {
            s.delete(o);
            tx.commit();
            s.close();
        } catch (Exception ex) {
            tx.rollback();
            s.close();
            throw ex;
        }
        return o;
    }

    public <T extends AbstractEntity> T fetchInstance(T o) {
        Session s = getSession();
        o = (T) s.get(o.getClass(), o.getId());
        s.close();
        return o;
    }

    public Session getSession() {
        LocalSessionFactoryBean sf = ctx.getBean(LocalSessionFactoryBean.class);
        return sf.getObject().openSession();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ctx = applicationContext;
    }

}
