package fi.datactor.commerce.api.dao;

import fi.datactor.commerce.api.model.dto.Log;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

;

/**
 * Created by rikuusi on 14.2.2017.
 */
public class LogDAO extends AbstractDAO {
    public Logger log = LoggerFactory.getLogger(this.getClass());
    public List<Log> retrieveAll() {
        Session s = getSession();
        List<Log> q = s.createQuery("FROM " + Log.class.getCanonicalName() + " ORDER BY date DESC").list();
        s.close();
        return q;
    }

}
