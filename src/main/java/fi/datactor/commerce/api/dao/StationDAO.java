package fi.datactor.commerce.api.dao;

import fi.datactor.commerce.api.model.dto.Log;
import fi.datactor.commerce.api.model.dto.Station;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by rikuusi on 14.2.2017.
 */
public class StationDAO extends AbstractDAO {
    public List<Station> retrieveAll() {
        Session s = getSession();
        List<Station> q = s.createQuery("FROM " + Station.class.getCanonicalName() + " ORDER BY name").list();
        s.close();
        return q;
    }

}
