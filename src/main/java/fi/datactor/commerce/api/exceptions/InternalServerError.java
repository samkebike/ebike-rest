/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datactor.commerce.api.exceptions;

/**
 * Internal Server Error
 * @author Riku Kuusisto
 */
public class InternalServerError extends AbstractControllerException {

    public InternalServerError(Object[] arguments, String messageCode, String message) {
        super(arguments, messageCode, message);
    }

    public InternalServerError(Object[] arguments, String messageCode, String message, Throwable cause) {
        super(arguments, messageCode, message, cause);
    }
    
}
