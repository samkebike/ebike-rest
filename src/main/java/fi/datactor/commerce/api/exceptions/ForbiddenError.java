/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datactor.commerce.api.exceptions;

/**
 * Resource Not Available Exception
 * @author Riku Kuusisto
 */
public class ForbiddenError extends AbstractControllerException {

        public ForbiddenError(Object[] arguments, String messageCode, String message) {
            super(arguments, messageCode, message);
        }

        public ForbiddenError(Object[] arguments, String messageCode, String message, Throwable cause) {
            super(arguments, messageCode, message, cause);
        }

}
