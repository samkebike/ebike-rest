/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datactor.commerce.impl.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.datactor.commerce.api.dao.*;
import fi.datactor.commerce.api.exceptions.AbstractControllerException;
import fi.datactor.commerce.api.exceptions.BadRequestError;
import fi.datactor.commerce.api.exceptions.InternalServerError;
import fi.datactor.commerce.api.model.Message;
import fi.datactor.commerce.api.model.dto.*;
import fi.datactor.commerce.api.model.request.TestRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

/**
 * External Data Service
 *
 * @author Riku Kuusisto
 */
@RestController
@Scope("prototype")
public class GetController extends AbstractController implements ApplicationContextAware{

    private final Logger log = LoggerFactory.getLogger(GetController.class);

    private final UUID id = UUID.randomUUID();

    public GetController() {
        log.debug("Creating interceptor");
    }




    @RequestMapping(value = "/api/test", method = {RequestMethod.POST}, produces = "application/json;charset=UTF-8")
    public Message<String> test(@RequestBody TestRequest data, HttpServletRequest request) throws AbstractControllerException, UnknownHostException {
        log.info("Run {}", id);


        log.info("IP Address: {}", request.getRemoteAddr());
        log.info("Host: {}", request.getRemoteHost());

        InetAddress address = InetAddress.getByName(request.getRemoteAddr());

        log.info("Bytes: {}", data.getId().toUpperCase());
        Message<String> response = new Message<>();
        response.setIdentification(id);
        response.setPayload("Card ID: " + data.getId().toUpperCase());
        return response;
    }

    @RequestMapping(value = "/api/get/log", method = {RequestMethod.GET,RequestMethod.POST}, produces = "application/json;charset=UTF-8")
    public List<Log> getLogs(){
        log.info("Run {}", id);
        LogDAO ldao = ctx.getBean("logDAO", LogDAO.class);
        return ldao.retrieveAll();
    }

    @RequestMapping(value = "/api/get/reservation", method = {RequestMethod.GET,RequestMethod.POST}, produces = "application/json;charset=UTF-8")
    public List<Reservation> getReservations(){
        log.info("Run {}", id);
        ReservationDAO dao = ctx.getBean("reservationDAO", ReservationDAO.class);
        return dao.retrieveAll();
    }

    @RequestMapping(value = "/api/get/reservation/active", method = {RequestMethod.GET,RequestMethod.POST}, produces = "application/json;charset=UTF-8")
    public List<Reservation> getActiveReservations(){
        log.info("Run {}", id);
        ReservationDAO dao = ctx.getBean("reservationDAO", ReservationDAO.class);
        return dao.retrieveActive();
    }

    /*
    @RequestMapping(value = "/api/get/customer", method = {RequestMethod.GET,RequestMethod.POST}, produces = "application/json;charset=UTF-8")
    public List<Customer> getCustomers(){
        log.info("Run {}", id);
        CustomerDAO dao = ctx.getBean("customerDAO", CustomerDAO.class);
        return dao.retrieveAll();
    }
    */

    @RequestMapping(value = "/api/get/customer", method = {RequestMethod.GET,RequestMethod.POST}, produces = "application/json;charset=UTF-8")
    public List<Customer> getCustomers(@RequestParam(value = "customerId", required = false) String customerId) throws AbstractControllerException {
        log.info("Run {}", id);
        try{
            CustomerDAO dao = ctx.getBean("customerDAO", CustomerDAO.class);
            if(customerId == null)
                return dao.retrieveAll();
            else
                return Collections.singletonList(dao.fetchInstance(new Customer(customerId)));

        } catch (Exception ex){
            log.info("Stack: {}", ex);
            throw new InternalServerError(new Object[]{}, "REST1000", ex.getMessage(), ex.getCause());
        }
    }

    @RequestMapping(value = "/api/get/station", method = {RequestMethod.GET,RequestMethod.POST}, produces = "application/json;charset=UTF-8")
    public List<Station> getStation(){
        log.info("Run {}", id);
        StationDAO dao = ctx.getBean("stationDAO", StationDAO.class);
        return dao.retrieveAll();
    }

    @RequestMapping(value = "/api/get/bike", method = {RequestMethod.GET,RequestMethod.POST}, produces = "application/json;charset=UTF-8")
    public List<Bike> getClient(@RequestParam(value = "station", required = false) String stationId) throws AbstractControllerException {
        log.info("Run {}", id);
        try{
            BikeDAO ldao = ctx.getBean("bikeDAO", BikeDAO.class);
            if(stationId == null)
                return ldao.retrieveAll();
            else
                return ldao.retrieveFrom(stationId);
        } catch (Exception ex){
            log.info("Stack: {}", ex);
            throw new InternalServerError(new Object[]{}, "REST1000", ex.getMessage(), ex.getCause());
        }
    }


    @RequestMapping(value = "/api/get/bike/available", method = {RequestMethod.POST}, produces = "application/json;charset=UTF-8")
    public List<Bike> getAvailableBikes() throws InternalServerError {
        try{
            BikeDAO ldao = ctx.getBean("bikeDAO", BikeDAO.class);
            List<Bike> bikes = ldao.retrieveAvailable();
            log.info("Bikes found {}", bikes.size());
            return bikes;
        } catch (Exception ex){
            log.info("Stack: {}", ex);
            throw new InternalServerError(new Object[]{}, "REST1000", ex.getMessage(), ex.getCause());
        }
    }

    @RequestMapping(value = "/api/get/access/{station}/{card}", method = {RequestMethod.GET,RequestMethod.POST}, produces = "application/json;charset=UTF-8")
    public Message<Integer> getAccess(@PathVariable("card") String cardId, @PathVariable("station") String stationId, HttpServletResponse response) throws AbstractControllerException {
        log.info("Run {}", id);
        if(stationId.equals("")){
            throw new BadRequestError(new Object[]{"station"}, "REST1001", "Station ID cannot be empty");
        }
        if(cardId.equals("")){
            throw new BadRequestError(new Object[]{"card"}, "REST1001", "Station ID cannot be empty");
        }
        try {
            CustomerDAO dao = ctx.getBean("customerDAO", CustomerDAO.class);
            Message<Integer> output = new Message<>();
            Customer c = dao.fetchInstanceByCardId(cardId.trim());
            Reservation r = c.getActiveReservation();
            if (r != null && r.getBike().getStation().getSId().equals(stationId)) {
                output.setPayload(1);
            } else {
                response.setStatus(403);
                output.setPayload(0);
            }
            return output;
        } catch (Exception ex){
            log.error("Stack: {}", ex);
            throw new InternalServerError(new Object[]{}, "REST1000", "Unexpected error occurred");
        }
    }


    /**
     * Gets Websphere AuthToken
     * @param request
     * @return 
     */
    private String getAuthToken(HttpServletRequest request) {
           Cookie[] cookies =  request.getCookies();
           if(cookies!=null) {
                for(Cookie cookie : cookies) {
                    if("LtpaToken2".equalsIgnoreCase(cookie.getName())) {
                        return cookie.getValue();
                    }
                }
           }
           return null;
    }
    
    public String requestToString(Object req) throws JsonProcessingException{
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(req);
                
    }

    
}
