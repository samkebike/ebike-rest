/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datactor.commerce.impl.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.datactor.commerce.api.dao.*;
import fi.datactor.commerce.api.exceptions.*;
import fi.datactor.commerce.api.model.Message;
import fi.datactor.commerce.api.model.Severity;
import fi.datactor.commerce.api.model.dto.*;
import fi.datactor.commerce.api.model.request.TestRequest;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

/**
 * External Data Service
 *
 * @author Riku Kuusisto
 */
@RestController
@Scope("prototype")
public class PostController extends AbstractController implements ApplicationContextAware {

    private final Logger log = LoggerFactory.getLogger(PostController.class);

    private final UUID id = UUID.randomUUID();

    public PostController() {
        log.debug("Creating interceptor");
    }


    @RequestMapping(value = "/api/post/log", method = {RequestMethod.POST}, produces = "application/json;charset=UTF-8")
    public Log postLog(@RequestBody TestRequest request) throws JsonProcessingException {
        log.info("Run {}", id);
        Log l = new Log();
        try {
            LogDAO dao = ctx.getBean("logDAO", LogDAO.class);
            BikeDAO bdao = ctx.getBean("bikeDAO", BikeDAO.class);
            CustomerDAO cdao = ctx.getBean("customerDAO", CustomerDAO.class);

            ObjectMapper mapper = new ObjectMapper();
            Bike b = bdao.fetchInstance(new Bike(request.getClient()));
            if(b == null){
                throw new BadRequestError(new Object[]{request.getClient()}, "REST1010", "No such bike");
            }
            l.setClient(b);

            String cardId = request.getId().trim();

            Customer c = cdao.fetchInstanceByCardId(cardId);
            if(c == null){
                throw new BadRequestError(new Object[]{cardId}, "REST1008", "No such customer " + cardId);
            }
            l.setCustomer(c);
            l.setSeverity(Severity.DEBUG);
            l.setMessage("Card " + cardId + " was read!");


            l = dao.insert(l);

        } catch (Exception ex) {
            log.error("Stack: {}", ex);

        }
        log.info("End Run {}", id);
        return l;
    }

    @RequestMapping(value = "/api/post/bike", method = {RequestMethod.POST}, produces = "application/json;charset=UTF-8")
    public Bike postBike(@RequestBody Bike request) throws DuplicateError {
        log.info("Run {}", id);
        BikeDAO dao = ctx.getBean("bikeDAO", BikeDAO.class);
        try {
            request = dao.insert(request);
        } catch (ConstraintViolationException ex) {
            throw new DuplicateError(new Object[]{}, "REST1007", "Bike already exists!");
        }

        return request;
    }

    @RequestMapping(value = "/api/post/station", method = {RequestMethod.POST}, produces = "application/json;charset=UTF-8")
    public Station postStation(@RequestBody Station request) throws DuplicateError {
        log.info("Run {}", id);
        log.info("location: {}", request.getLocation());
        log.info("name: {}", request.getName());
        StationDAO dao = ctx.getBean("stationDAO", StationDAO.class);
        try {
            request = dao.insert(request);
        } catch (ConstraintViolationException ex) {
            throw new DuplicateError(new Object[]{}, "REST1006", "Station already exists!");
        }
        return request;
    }

    @RequestMapping(value = "/api/post/customer", method = {RequestMethod.POST}, produces = "application/json;charset=UTF-8")
    public Customer postCustomer(@RequestBody Customer request) throws DuplicateError {
        log.info("Run {}", id);
        CustomerDAO dao = ctx.getBean("customerDAO", CustomerDAO.class);
        try {
            request = dao.insert(request);
        } catch (ConstraintViolationException ex) {
            throw new DuplicateError(new Object[]{}, "REST1005", "Customer already exists!");
        }
        return request;
    }

    @RequestMapping(value = "/api/post/reserve", method = {RequestMethod.POST}, produces = "application/json;charset=UTF-8")
    public Message<String> reserveBike(@RequestBody Reservation reservation) throws AbstractControllerException {
        Message<String> output = new Message<>();
        CommonDAO dao = (CommonDAO) ctx.getBean("commonDAO");
        Bike b = dao.fetchInstance(reservation.getBike());
        if (b == null) {
            throw new ResourceNotFoundError(new Object[]{reservation.getBike().getSId()}, "REST404", "No such bike");
        }
        if (b.getLocked() != (byte) 0) {
            throw new ForbiddenError(new Object[]{}, "REST1003", "Bike is not available");
        }

        Customer c = dao.fetchInstance(reservation.getCustomer());
        if (c == null) {
            throw new BadRequestError(new Object[]{reservation.getCustomer().getSId()}, "REST1008", "No record found");
        }
        if (c.getActiveReservation() != null) {
            throw new BadRequestError(new Object[]{}, "REST1009", "Customer already has a reservation");
        }
        b.setLocked((byte) 1);
        reservation.setActive(true);

        Session s = dao.getSession();
        Transaction tx = s.beginTransaction();
        try {
            dao.insert(reservation, s);
            dao.edit(b, s);
            tx.commit();
        } catch (Exception ex) {
            log.error("Stack: {}", ex);
            tx.rollback();
        } finally {
            if (s.isConnected())
                s.close();
        }
        output.setPayload("Reserved " + b.getNr());
        return output;
    }

    @RequestMapping(value = "/api/post/release/{resId}", method = {RequestMethod.POST}, produces = "application/json;charset=UTF-8")
    public Message<String> releaseBike(@PathVariable("resId") String resId) throws ResourceNotFoundError, ForbiddenError {
        Message<String> output = new Message<>();
        CommonDAO dao = (CommonDAO) ctx.getBean("commonDAO");
        Reservation res = new Reservation(resId);
        res = dao.fetchInstance(res);

        Customer c = new Customer(res.getCustomer().getSId());
        c = dao.fetchInstance(c);
        Reservation reservation = c.getActiveReservation();


        Bike b = dao.fetchInstance(reservation.getBike());
        if (b == null) {
            throw new ResourceNotFoundError(new Object[]{}, "REST404", "No such bike");
        }
        b.setLocked((byte) 0);
        reservation.setActive(false);

        Session s = dao.getSession();
        Transaction tx = s.beginTransaction();
        try {
            dao.edit(reservation, s);
            dao.edit(b, s);
            tx.commit();
        } catch (Exception ex) {
            log.error("Stack: {}", ex);
            tx.rollback();
        } finally {
            if (s.isConnected())
                s.close();
        }
        output.setPayload("Released " + b.getNr());
        return output;
    }

    @RequestMapping(value = "/api/post/cardid", method = {RequestMethod.POST}, produces = "application/json;charset=UTF-8")
    public Message<String> changeCardId(@RequestBody Customer request) throws DuplicateError {
        log.info("Run {}", id);
        Message<String> output = new Message<>();
        CustomerDAO dao = ctx.getBean("customerDAO", CustomerDAO.class);
        Customer c = new Customer(request.getSId());
        c = dao.fetchInstance(c);
        c.setCardId(request.getCardId());

        Session s = dao.getSession();
        Transaction tx = s.beginTransaction();
        try {
            dao.edit(c, s);
            tx.commit();
        } catch (Exception ex) {
            log.error("Stack: {}", ex);
            tx.rollback();
        } finally {
            if (s.isConnected())
                s.close();
        }
        output.setPayload("Changed cardId to " + c.getCardId());
        return output;
    }
}
