package fi.datactor.commerce.impl.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.datactor.commerce.api.exceptions.*;
import fi.datactor.commerce.api.model.error.ErrorMessage;
import fi.datactor.commerce.api.model.error.UnderlyingDetails;
import fi.datactor.commerce.api.model.error.UserMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by rikuusi on 14.2.2017.
 */
public class AbstractController implements ApplicationContextAware {



    @Autowired
    protected Locale locale;
    protected ApplicationContext ctx;
    Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * Sets Localization
     *
     * @param locale Localization
     */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    /**
     * Internal Server Error
     *
     * @param req
     * @param ex
     * @return
     */
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(InternalServerError.class)
    public ErrorMessage handleError(HttpServletRequest req, InternalServerError ex) {
        ErrorMessage message = constructMessage(req, ex);

        return message;
    }


    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(ResourceNotFoundError.class)
    public ErrorMessage handleError(HttpServletRequest req, ResourceNotFoundError ex) {
        ErrorMessage message = constructMessage(req, ex);

        return message;
    }

    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    @ExceptionHandler(ForbiddenError.class)
    public ErrorMessage handleError(HttpServletRequest req, ForbiddenError ex) {
        ErrorMessage message = constructMessage(req, ex);
        return message;
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(DuplicateError.class)
    public ErrorMessage handleError(HttpServletRequest req, DuplicateError ex) {
        ErrorMessage message = constructMessage(req, ex);
        return message;
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadRequestError.class)
    public ErrorMessage handleError(HttpServletRequest req, BadRequestError ex) {
        ErrorMessage message = constructMessage(req, ex);
        return message;
    }

    public ErrorMessage constructMessage(HttpServletRequest req, AbstractControllerException ex){
        log.info("Constructing error message");
        ErrorMessage message = new ErrorMessage();

        UnderlyingDetails details = new UnderlyingDetails();
        message.setUnderlyingDetails(details);
        List<String> data = new ArrayList<String>();
        details.setCauses(data);
        data.add(ex.getMessageCode());
        data.add(ex.getMessage());

        UserMessage userMessage = new UserMessage();

        //Fetching message that is displayed for the user
        userMessage.setText(ctx.getMessage(ex.getMessageCode(), ex.getArguments(), locale));

        message.setUserMessage(userMessage);

        return message;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ctx = applicationContext;
    }

    public String requestToString(Object req) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(req);

    }

    public String getBody(HttpServletRequest request) throws IOException {

        String body = null;
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;

        try {
            InputStream inputStream = request.getInputStream();
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                char[] charBuffer = new char[128];
                int bytesRead = -1;
                while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                    stringBuilder.append(charBuffer, 0, bytesRead);
                }
            } else {
                stringBuilder.append("");
            }
        } catch (IOException ex) {
            throw ex;
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                    throw ex;
                }
            }
        }

        body = stringBuilder.toString();
        return body;
    }

}
