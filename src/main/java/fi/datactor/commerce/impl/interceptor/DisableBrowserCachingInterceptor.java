/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datactor.commerce.impl.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This piece of code should force Pragma no cache header
 * @author Riku Kuusisto
 */
public class DisableBrowserCachingInterceptor extends HandlerInterceptorAdapter{

    public DisableBrowserCachingInterceptor() {
        log.debug("Creating interceptor");
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.debug("PreHandle");
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate, age=0"); // HTTP 1.1
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0
        response.setDateHeader("Expires", 0); // Proxies
        return super.preHandle(request, response, handler); //To change body of generated methods, choose Tools | Templates.
    }

    private final Logger log = LoggerFactory.getLogger(DisableBrowserCachingInterceptor.class);

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
       log.debug("PostHandle");
       super.postHandle(request, response, handler, modelAndView);
        
    }

}
