<%-- 
    Document   : hello
    Created on : Nov 8, 2015, 1:55:36 PM
    Author     : mikam
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <spring:url value="/resources/css/style.css" var="mainCss" />
        <spring:url value="/resources/images/logo.png" var="logo"/>
        <spring:url value="/resources/images/addressbar.ico" var="barIco" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>EDS-Controller</title>

        <link href="${mainCss}" rel="stylesheet" />
        <link rel="shortcut icon" href="${barIco}">
    </head>
    <body>
        <img src="${logo}" alt="Logo" style="width: 400px;" >
        <h2 class="default">EDS-Service end-points:</h2>
  
        <table>
            <thead>
                <tr>
                    <th>Path</th>
                    <th>Type</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${endPoints}" var="endpoint"> 
                 <tr>
                     <c:if test="${endpoint.patternsCondition != '[/]'}">
                     <td>${endpoint.patternsCondition}</td>
                     <td>${endpoint.methodsCondition}</td>
                     </c:if>
                </tr>               
            </c:forEach>
        </tbody>
        </table>
    </body>


</html>
